# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.cluster import KMeans

data = pd.read_csv("WHO_Suicide_Statistics_New.csv")
data.loc[data["age"] == "5-14 years", "age"] = 0
data.loc[data["age"] == "15-24 years", "age"] = 1
data.loc[data["age"] == "25-34 years", "age"] = 2
data.loc[data["age"] == "35-54 years", "age"] = 3
data.loc[data["age"] == "55-74 years", "age"] = 4
data.loc[data["age"] == "75+ years", "age"] = 5


X = pd.DataFrame(data, columns=["suicides_no", "population"], dtype=float)
y = pd.DataFrame(data, columns=["age"])

colors = np.array(["red", "green", "blue", "yellow", "pink", "brown"])
fig, axes = plt.subplots(1, 2, figsize=(10, 3))

axes[0].scatter(X["suicides_no"], X["population"], c=colors[y["age"]], edgecolor='black', cmap='jet', s=10)
axes[0].set_title('PRZED')
axes[0].grid(True)

nc3 = KMeans(n_clusters=6)
nc3.fit(X)
pre = nc3.labels_

axes[1].scatter(X["suicides_no"], X["population"], c=colors[pre], edgecolor='black', cmap='jet', s=10)
axes[1].set_title('PO')
axes[1].grid(True)

fig.tight_layout()
plt.show()

# X.fillna(method ='ffill', inplace = True)
# print(X.head())
# scaler = StandardScaler()
# X_scaled = scaler.fit_transform(X)
# X_normalized = normalize(X_scaled)
# X_normalized = pd.DataFrame(X_normalized)
# pca = PCA(n_components = 2)
# X_principal = pca.fit_transform(X_normalized)
# X_principal = pd.DataFrame(X_principal)
# X_principal.columns = ['P1', 'P2']
# print(X_principal.head())
#
# db_default = DBSCAN(eps = 0.0375, min_samples = 3).fit(X_principal)
# labels = db_default.labels_
#
# colours = {}
# colours[0] = 'r'
# colours[1] = 'g'
# colours[2] = 'b'
# colours[-1] = 'k'
#
# # Building the colour vector for each data point
# cvec = [colours[label] for label in labels]
#
# # For the construction of the legend of the plot
# r = plt.scatter(X_principal['P1'], X_principal['P2'], color='r');
# g = plt.scatter(X_principal['P1'], X_principal['P2'], color='g');
# b = plt.scatter(X_principal['P1'], X_principal['P2'], color='b');
# k = plt.scatter(X_principal['P1'], X_principal['P2'], color='k');
#
# # Plotting P1 on the X-Axis and P2 on the Y-Axis
# # according to the colour vector defined
# plt.figure(figsize=(9, 9))
# plt.scatter(X_principal['P1'], X_principal['P2'], c=cvec)
#
# # Building the legend
# plt.legend((r, g, b, k), ('Label 0', 'Label 1', 'Label 2', 'Label -1'))
#
# plt.show()