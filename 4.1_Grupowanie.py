# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from matplotlib.colors import ListedColormap
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.svm import SVC

data = pd.read_csv("WHO_Suicide_Statistics_New.csv")
data.loc[data["age"] == "5-14 years", "age"] = 0
data.loc[data["age"] == "15-24 years", "age"] = 1
data.loc[data["age"] == "25-34 years", "age"] = 2
data.loc[data["age"] == "35-54 years", "age"] = 3
data.loc[data["age"] == "55-74 years", "age"] = 4
data.loc[data["age"] == "75+ years", "age"] = 5

training_set, test_set = train_test_split(data, test_size = 0.2, random_state = 1)

X = pd.DataFrame(data, columns=["suicides_no", "population"], dtype=float)
y = pd.DataFrame(data, columns=["age"])

X_train = training_set.iloc[:, [False, False, False, True, False, False]].values
Y_train = training_set.iloc[:, [False, False, False, True, False, False]].values
X_test = test_set.iloc[:, [False, False, False, True, False, False]].values
Y_test = test_set.iloc[:, [False, False, False, False, True, True]].values

le = LabelEncoder()
Y_train = le.fit_transform(Y_train)

classifier = SVC(kernel='rbf', random_state = 1)
classifier.fit(X_train,Y_train)

Y_pred = classifier.predict(X_test)

test_set["age"] = Y_pred

plt.figure(figsize = (7,7))
X_set, y_set = X_train, Y_train
X1, X2 = np.meshgrid(np.arange(start = X_set[:, 0].min() - 1, stop = X_set[:, 0].max() + 1, step = 0.01), np.arange(start = X_set[:, 1].min() - 1, stop = X_set[:, 1].max() + 1, step = 0.01))
plt.contourf(X1, X2, classifier.predict(np.array([X1.ravel(), X2.ravel()]).T).reshape(X1.shape), alpha = 0.75, cmap = ListedColormap(('black', 'white')))
plt.xlim(X1.min(), X1.max())
plt.ylim(X2.min(), X2.max())
for i, j in enumerate(np.unique(y_set)):
    plt.scatter(X_set[y_set == j, 0], X_set[y_set == j, 1], c = ListedColormap(('red', 'orange'))(i), label = j)
plt.title('Apples Vs Oranges')
plt.xlabel('Weight In Grams')
plt.ylabel('Size in cm')
plt.legend()
plt.show()