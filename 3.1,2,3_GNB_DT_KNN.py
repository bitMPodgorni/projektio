# -*- coding: utf-8 -*-
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn import metrics
from sklearn.metrics import confusion_matrix
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier, plot_tree
from sklearn.utils import shuffle

data = pd.read_csv("WHO_Suicide_Statistics_New.csv")

all_inputs = data[["suicides_no", "population"]].values
all_classes = data['sex'].values
(trainX, testX, trainY, testY) = train_test_split(all_inputs, all_classes, train_size=0.20, random_state=20)

print("###   Gaussian Naive Bayes   ###")
gnb = GaussianNB()
gnb.fit(trainX, trainY)
predict_gnb = gnb.predict(testX)
confusion_matrix_gnb = confusion_matrix(testY, predict_gnb)
accuracy_score_gnb = metrics.accuracy_score(testY, predict_gnb)
print()
print('Confusion_Matrix dla Bayessa(gnb): \n', confusion_matrix_gnb)
print("Dokladnosc: {:.0%}".format(accuracy_score_gnb))
print()

print("###   Decision Tree Classifier   ###")
dtc = DecisionTreeClassifier()
dtc.fit(trainX, trainY)
predict_dtc = dtc.predict(testX)
accuracy_score_dtc = metrics.accuracy_score(testY, predict_dtc)
print()
print('Confusion_Matrix dla Drzewa(dtc): \n', confusion_matrix(testY, predict_dtc))
print("Dokladnosc: {:.0%}".format(accuracy_score_dtc))
print()

print("###   K Nearest Neighbor (k=3)   ###")
knn_3 = KNeighborsClassifier(n_neighbors=3, metric='euclidean')
knn_3.fit(trainX, trainY)
predict_knn3 = knn_3.predict(testX)
confusion_matrix_knn3 = confusion_matrix(testY, predict_knn3)
accuracy_score_knn3 = metrics.accuracy_score(testY, predict_knn3)
print()
print('Confusion_Matrix dla k-NN(k=3): \n', confusion_matrix_knn3)
print("Dokladnosc: {:.0%}".format(accuracy_score_knn3))
print()

print("###   K Nearest Neighbor (k=5)   ###")
knn_5 = KNeighborsClassifier(n_neighbors=5, metric='euclidean')
knn_5.fit(trainX, trainY)
predict_knn5 = knn_5.predict(testX)
confusion_matrix_knn5 = confusion_matrix(testY, predict_knn5)
accuracy_score_knn5 = metrics.accuracy_score(testY, predict_knn5)
print()
print('Confusion_Matrix dla k-NN(k=5): \n', confusion_matrix_knn5)
print("Dokladnosc: {:.0%}".format(accuracy_score_knn5))
print()

print("###   K Nearest Neighbor (k=11)   ###")
knn_11 = KNeighborsClassifier(n_neighbors=11, metric='euclidean')
knn_11.fit(trainX, trainY)
predict_knn11 = knn_11.predict(testX)
confusion_matrix_knn11 = confusion_matrix(testY, predict_knn11)
accuracy_score_knn11 = metrics.accuracy_score(testY, predict_knn11)
print()
print('Confusion_Matrix dla k-NN(k=11): \n', confusion_matrix_knn11)
print("Dokladnosc: {:.0%}".format(accuracy_score_knn11))
print()

print("###   WYKRES   ###")
values = [
    int(accuracy_score_gnb * 100),
    int(accuracy_score_dtc * 100),
    int(accuracy_score_knn3 * 100),
    int(accuracy_score_knn5 * 100),
    int(accuracy_score_knn11 * 100)]
legend = [
    'Gaussian Naive Bayes',
    'Decision Tree Classifier',
    'K Nearest Neighbor (3)',
    'K Nearest Neighbor (5)',
    'K Nearest Neighbor (11)']

fig, ax = plt.subplots()
plt.title('Porownanie klasyfikatorow [w %]')
plt.bar(legend, values)
plt.yticks(range(0, 105, 5))
plt.xticks(legend)
plt.grid()

plt.show()