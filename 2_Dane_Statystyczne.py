# -*- coding: utf-8 -*-
import pandas as pd
import collections as c

data = pd.read_csv("WHO_Suicide_Statistics_New.csv")

print("## ŚREDNIA ##")
for row in ["year", "suicides_no", "population"]:
    print("Średnia dla kolumny '" + row.__str__() + "' wynosi: " + data[row].mean().__str__())

print("\n## ODCHYLENIE STANDARDOWE ##")
for row in ["year", "suicides_no", "population"]:
    print("Odchylenie standardowe dla kolumny '" + row.__str__() + "' wynosi: " + data[row].std().__str__())

print("\n## MINIMUM ##")
for row in ["year", "suicides_no", "population"]:
    print("Minimum dla kolumny '" + row.__str__() + "' wynosi: " + data[row].min().__str__())

print("\n## MAKSIMUM ##")
for row in ["year", "suicides_no", "population"]:
    print("Maksimum dla kolumny '" + row.__str__() + "' wynosi: " + data[row].max().__str__())



print("\n## NAJWIEKSZA i NAJMNIEJSZA ILOSC DANYCH O KRAJU: ")
cnt = c.Counter()
for word in data["country"]:
    cnt[word] += 1
print(cnt)
print(str(max(cnt)) + ' wystąpień: ' + str(max(cnt.values())))
print(str(min(cnt)) + ' wystąpień: ' + str(min(cnt.values())))

print("\n## NAJWIEKSZA i NAJMNIEJSZA ILOSC DANYCH O PŁCI: ")
cnt = c.Counter()
for word in data["sex"]:
    cnt[word] += 1
print(cnt)
print(str(max(cnt)) + ' wystąpień: ' + str(max(cnt.values())))
print(str(min(cnt)) + ' wystąpień: ' + str(min(cnt.values())))

print("\n## NAJWIEKSZA i NAJMNIEJSZA ILOSC DANYCH O WIEKU: ")
cnt = c.Counter()
for word in data["age"]:
    cnt[word] += 1
print(cnt)
print(str(max(cnt)) + ' wystąpień: ' + str(max(cnt.values())))
print(str(min(cnt)) + ' wystąpień: ' + str(min(cnt.values())))

print("\n## NAJWIEKSZA i NAJMNIEJSZA ILOSC DANYCH Z ROKU: ")
cnt = c.Counter()
for word in data["year"]:
    cnt[word] += 1
print(cnt)
print(str(max(cnt)) + ' wystąpień: ' + str(max(cnt.values())))
print(str(min(cnt)) + ' wystąpień: ' + str(min(cnt.values())))

