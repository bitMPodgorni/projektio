# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.cluster import KMeans
import seaborn as sns
from apyori import apriori
# from mlxtend.frequent_patterns import apriori

da = pd.read_csv("WHO_Suicide_Statistics_New.csv")
transactions = [
    ['sex', "male"]
    ]

final = apriori(da)
result = list(da)
print(len(result))
print(result[0])


sns.countplot(x = 'suicides_no', data = da, order = da['sex'].value_counts().iloc[:10].index)
plt.xticks(rotation=90)
plt.show()