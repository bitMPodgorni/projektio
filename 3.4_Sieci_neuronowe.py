# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
import tensorflow
from sklearn.preprocessing import normalize
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.utils import np_utils
from sklearn.metrics import confusion_matrix

data = pd.read_csv("WHO_Suicide_Statistics_New.csv")
data.reindex()

data.loc[data["sex"] == "male", "sex"] = 0
data.loc[data["sex"] == "female", "sex"] = 1
print(data.head())

data = data.iloc[np.random.permutation(len(data))]
print(data.head())

X = data.iloc[:, [False, False, False, False, True, True]].values
print(X)
y = data.iloc[:, [False, False, True, False, False, False]].values
print(y)

X_normalized = normalize(X, axis=0)

#Creating train,test and validation data
total_length = len(data)
train_length = int(0.2 * total_length)
test_length = int(0.8 * total_length)

X_train = X_normalized[:train_length]
X_test = X_normalized[train_length:]
y_train = y[:train_length]
y_test = y[train_length:]
#y_test_2 = y_test



#Change the label to one hot vector
y_train = np_utils.to_categorical(y_train, num_classes=2)
y_test = np_utils.to_categorical(y_test, num_classes=2)

model=Sequential()
model.add(Dense(1000, input_dim=2, activation='relu'))
model.add(Dense(500, activation='relu'))
model.add(Dense(300, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(2, activation='softmax'))
model.compile(
    loss='categorical_crossentropy',
    optimizer='adam',
    metrics=['accuracy'])

print('Neural Network Model Summary: ')
model.summary()

# Train the model
model.fit(X_train, y_train, validation_data=(X_test, y_test), verbose=1, batch_size=20, epochs=5)

# Test on unseen data
results = model.evaluate(X_test, y_test)

print('Strata: ' + str(results[0]*100))
print('Dokładność: ' + str(results[1]*100))

prediction = model.predict(X_test)

y_label = np.argmax(y_test, axis=1)
predict_label = np.argmax(prediction, axis=1)

print(confusion_matrix(y_label, predict_label))