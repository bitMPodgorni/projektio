# -*- coding: utf-8 -*-
import pandas as pd

missing_values = ["n/a", "na", "--", "-", "", " ", 0, 0.0]
df = pd.read_csv("WHO_Suicide_Statistics.csv", na_values=missing_values)

print("### ORYGINALNA BAZA ###")
print("Błędy w poszczególnych kolumnach: \n" + df.isnull().sum().__str__())
print("Błędów razem: " + df.isnull().sum().sum().__str__())
print()

print("### OCZYSZCZANIE DANYCH ###")
data = df.dropna().reset_index(drop=True)
data.to_csv('WHO_Suicide_Statistics_New.csv', index=False)
print(data.isnull().sum())
print("Bledow razem: " + data.isnull().sum().sum().__str__())

print(data["age"].unique())